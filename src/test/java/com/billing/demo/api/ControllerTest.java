package com.billing.demo.api;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.billing.demo.Bill;
import com.billing.demo.Controller;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(value = Controller.class, secure = false)
public class ControllerTest {

	@Autowired
	private MockMvc mock;

	@MockBean
	private Controller controller;

	@Test
	public void billExceedRM100() throws Exception {

		Bill bill = new Bill();
		bill.setBill(230.99);
		bill.setGst(6.9);
		bill.setService_charge(10.0);
		bill.setPerson(2);

		Mockito.when(controller.split(Mockito.any(Bill.class))).thenCallRealMethod();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/bill/split")
				.accept(MediaType.APPLICATION_JSON).content(ControllerTest.toJson(bill))
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mock.perform(requestBuilder).andReturn();
		String content = result.getResponse().getContentAsString();
		String expected = "{\"each_payable\":129.24}";
		assertEquals(expected, content);

		// Check 
		System.out.println("\nbillExceedRM100");
		System.out.println("Json Request: " + ControllerTest.toJson(bill));
		System.out.println("Json response: " + content);
	}

	@Test
	public void billExceedRM500() throws Exception {

		Bill bill = new Bill();
		bill.setBill(699.22);
		bill.setGst(7.5);
		bill.setService_charge(9);
		bill.setPerson(4);

		Mockito.when(controller.split(Mockito.any(Bill.class))).thenCallRealMethod();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/bill/split")
				.accept(MediaType.APPLICATION_JSON).content(ControllerTest.toJson(bill))
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mock.perform(requestBuilder).andReturn();
		String content = result.getResponse().getContentAsString();
		String expected = "{\"each_payable\":187.92}";
		assertEquals(expected, content);

		// Check 
		System.out.println("\nbillExceedRM500");
		System.out.println("Json Request: " + ControllerTest.toJson(bill));
		System.out.println("Json response: " + content);
	}

	@Test
	public void MoreThan10People() throws Exception {

		Bill bill = new Bill();
		bill.setBill(111.11);
		bill.setGst(33.99);
		bill.setService_charge(10.0);
		bill.setPerson(20);

		Mockito.when(controller.split(Mockito.any(Bill.class))).thenCallRealMethod();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/bill/split")
				.accept(MediaType.APPLICATION_JSON).content(ControllerTest.toJson(bill))
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mock.perform(requestBuilder).andReturn();
		String content = result.getResponse().getContentAsString();
		String expected = "{\"each_payable\":7.61}"; 
		assertEquals(expected, content);

		// Check 
		System.out.println("\nMoreThan10People");
		System.out.println("Json Request: " + ControllerTest.toJson(bill));
		System.out.println("Json response: " + content);
	}

	@Test
	public void MixScenario() throws Exception {

		Bill bill = new Bill();
		bill.setBill(999.99);
		bill.setGst(7.5);
		bill.setService_charge(10.0);
		bill.setPerson(20);

		Mockito.when(controller.split(Mockito.any(Bill.class))).thenCallRealMethod();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/bill/split")
				.accept(MediaType.APPLICATION_JSON).content(ControllerTest.toJson(bill))
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mock.perform(requestBuilder).andReturn();
		String content = result.getResponse().getContentAsString();
		String expected = "{\"each_payable\":52.75}";
		assertEquals(expected, content);

		System.out.println("\nMixScenario");
		System.out.println("Json Request: " + ControllerTest.toJson(bill));
		System.out.println("Json response: " + content);
	}

	@Test
	public void TestBillInvalidInput() throws Exception {

		Bill bill = new Bill();
		bill.setBill(0);
		bill.setGst(7.5);
		bill.setService_charge(9);
		bill.setPerson(20);

		Mockito.when(controller.split(Mockito.any(Bill.class))).thenCallRealMethod();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/bill/split")
				.accept(MediaType.APPLICATION_JSON).content(ControllerTest.toJson(bill))
				.contentType(MediaType.APPLICATION_JSON);

		// Check HTTP Error Code 400
		mock.perform(requestBuilder).andExpect(status().isBadRequest());
		MvcResult result = mock.perform(requestBuilder).andReturn();
		String content = result.getResponse().getContentAsString();
		String expected = "Error Message Code 400: Invalid input detected!";
		assertEquals(expected, content);

		System.out.println("\nTestBillInvalidInput");
		System.out.println("Json Request: " + ControllerTest.toJson(bill));
		System.out.println("Json response: " + content);
	}

	@Test
	public void TestGSTInvalidInput() throws Exception {

		Bill bill = new Bill();
		bill.setBill(878);
		bill.setGst(-1);
		bill.setService_charge(9);
		bill.setPerson(20);

		Mockito.when(controller.split(Mockito.any(Bill.class))).thenCallRealMethod();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/bill/split")
				.accept(MediaType.APPLICATION_JSON).content(ControllerTest.toJson(bill))
				.contentType(MediaType.APPLICATION_JSON);

		// Check HTTP Error Code 400
		mock.perform(requestBuilder).andExpect(status().isBadRequest());
		MvcResult result = mock.perform(requestBuilder).andReturn();
		String content = result.getResponse().getContentAsString();
		String expected = "Error Message Code 400: Invalid input detected!";
		assertEquals(expected, content);

		System.out.println("\nTestGSTInvalidInput");
		System.out.println("Json Request: " + ControllerTest.toJson(bill));
		System.out.println("Json response: " + content);
	}

	@Test
	public void TestServiceChargeInvalidInput() throws Exception {

		Bill bill = new Bill();
		bill.setBill(878);
		bill.setGst(4);
		bill.setService_charge(-33.33);
		bill.setPerson(20);

		Mockito.when(controller.split(Mockito.any(Bill.class))).thenCallRealMethod();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/bill/split")
				.accept(MediaType.APPLICATION_JSON).content(ControllerTest.toJson(bill))
				.contentType(MediaType.APPLICATION_JSON);

		// Check HTTP Error Code 400
		mock.perform(requestBuilder).andExpect(status().isBadRequest());
		MvcResult result = mock.perform(requestBuilder).andReturn();
		String content = result.getResponse().getContentAsString();
		String expected = "Error Message Code 400: Invalid input detected!";
		assertEquals(expected, content);

		System.out.println("\nTestServiceChargeInvalidInput");
		System.out.println("Json Request: " + ControllerTest.toJson(bill));
		System.out.println("Json response: " + content);
	}

	@Test
	public void TestPersonInvalidInput() throws Exception {

		Bill bill = new Bill();
		bill.setBill(666);
		bill.setGst(2);
		bill.setService_charge(33);
		bill.setPerson(0);

		Mockito.when(controller.split(Mockito.any(Bill.class))).thenCallRealMethod();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/bill/split")
				.accept(MediaType.APPLICATION_JSON).content(ControllerTest.toJson(bill))
				.contentType(MediaType.APPLICATION_JSON);

		// Check HTTP Error Code 400
		mock.perform(requestBuilder).andExpect(status().isBadRequest());
		MvcResult result = mock.perform(requestBuilder).andReturn();
		String content = result.getResponse().getContentAsString();
		String expected = "Error Message Code 400: Invalid input detected!";
		assertEquals(expected, content);

		System.out.println("\nTestPersonInvalidInput");
		System.out.println("Json Request: " + ControllerTest.toJson(bill));
		System.out.println("Json response: " + content);
	}

	@Test
	public void TestRequestURLError404() throws Exception {

		Bill bill = new Bill();
		bill.setBill(666);
		bill.setGst(2);
		bill.setService_charge(33);
		bill.setPerson(0);

		Mockito.when(controller.split(Mockito.any(Bill.class))).thenCallRealMethod();

		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/bill/test")
				.accept(MediaType.APPLICATION_JSON).content(ControllerTest.toJson(bill))
				.contentType(MediaType.APPLICATION_JSON);

		// Check HTTP Error Code 404
		mock.perform(requestBuilder).andExpect(status().isNotFound());

	}

	public static String toJson(Object object) throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}
}
