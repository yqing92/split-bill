package com.billing.demo;

import java.text.ParseException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@RequestMapping(value = "/bill/split", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> split(@RequestBody Bill b) throws ParseException {

		Double bill, gst, service_charge;
		int person;
		String result;

		bill = b.getBill();
		gst = b.getGst();
		service_charge = b.getService_charge();
		person = b.getPerson();
		result = b.Calculate(bill, gst, service_charge, person);

		if (result.equalsIgnoreCase("invalid")) {
			return new ResponseEntity<String>("Error Message Code 400: Invalid input detected!", HttpStatus.BAD_REQUEST);
		} else {
			return ResponseEntity.accepted().body(result);
		}
	}
}
