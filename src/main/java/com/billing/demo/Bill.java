package com.billing.demo;

import java.text.ParseException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class Bill {

	private double gst;
	private int person;
	private double bill;
	private double service_charge;
	// private double each_payable;

	public Bill() {

	}

	public Bill(double bill, double gst, double service_charge, int person) throws ParseException {
		this.gst = gst;
		this.bill = bill;
		this.service_charge = service_charge;
		this.person = person;
	}

	public double getGst() throws ParseException {
		gst = RoundOff(gst);
		return gst;
	}

	public void setGst(double gst) throws ParseException {
		gst = RoundOff(gst);
		this.gst = gst;
	}

	public int getPerson() {
		return person;
	}

	public void setPerson(int person) {
		this.person = person;
	}

	public double getBill() throws ParseException {
		bill = RoundOff(bill);
		return bill;
	}

	public void setBill(double bill) throws ParseException {
		bill = RoundOff(bill);
		this.bill = bill;
	}

	public double getService_charge() throws ParseException {
		service_charge = RoundOff(service_charge);
		return service_charge;
	}

	public void setService_charge(double service_charge) throws ParseException {
		service_charge = RoundOff(service_charge);
		this.service_charge = service_charge;
	}

	public String Calculate(double bill, double gst, double service_charge, int person) throws ParseException {

		double total;
		ObjectNode node;

		if (bill <= 0 || gst < 0 || service_charge < 0 || person < 1) { // Check invalid inputs
			return "invalid";
		} else {

			if (bill > 500) {// no service charge
				service_charge = 0;
			} else if (bill > 100) {// Reduce service charge by 50%
				service_charge = service_charge * 50 / 100;
			}

			if (person > 10) {// subtract gst by 2
				gst = gst - 2;
			}

			// gst and service charge refer to original price
			total = (bill * (1 + (gst / 100 + service_charge / 100))) / person;
			total = RoundOff(total);

			ObjectMapper mapper = new ObjectMapper();
			node = mapper.createObjectNode();
			node.put("each_payable", total);
			return node.toString();
		}
	}

	public double RoundOff(double a) throws ParseException {
		double result = 0;
		result = (double) Math.round(a * 100) / 100;
		return result;
	}
}
